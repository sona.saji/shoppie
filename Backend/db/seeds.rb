# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Product.create([
  {name: "Parker Pen", description:"Branded pen, black ink and high quality material", price: 25.0},
  {name: "Camel Pencil", description:"Coloring Pencil with 12 different colors", price: 55.0},
  {name: "Camel coloring book", description:"Coloring Book which id good for childrens", price: 135.0},
  {name: "Allen Solly Shirt", description:"Branded Shirt with blue strips design", price: 245.0}])