Rails.application.routes.draw do
  
  devise_for :users,
  controllers: {
    sessions: 'api/v1/sessions',
    registrations: 'api/v1/registrations'
  }
  namespace :api do
    namespace :v1 do
      resources :users, except: %i[ create ]
      resources :products
      resources :cart_items
      resources :carts, only: %i[ show ]do
        member do
          get 'total_price'
        end
      end
      resources :categories
      # end
    end
  end
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "api/v1/products#index"
end
