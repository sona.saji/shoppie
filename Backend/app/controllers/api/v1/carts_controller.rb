class Api::V1::CartsController < ApplicationController
  before_action :set_cart, only: %i[ show total_price ]

  #show user cart with cart items
  def show
    @cart_items = @cart.cart_items.order('created_at desc')
    @cart_items_data = @cart_items.map{|item|[ item: item, product: item.product]}.flatten
    render json: @cart_items_data
  end

  #find total price of product in a cart
  def total_price
    @total_price = @cart.products_total_price
    render json: @total_price
  end

  private

    #find cart
    def set_cart
      @cart = Cart.find(params[:id])
    end
end
