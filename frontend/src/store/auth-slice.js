import { createSlice } from "@reduxjs/toolkit";

const initialAuthState = {
    isAuthenticated: localStorage.getItem("userData") ? true : false,
    authUser: localStorage.getItem("userData") ? (JSON.parse(localStorage.getItem("userData"))).data : null
};

const authSlice = createSlice({
    name: 'authentication',
    initialState: initialAuthState,
    reducers: {
        login(state, action) {
            state.isAuthenticated = true;
            state.authUser = action.payload;
        },
        logout(state) {
            state.isAuthenticated = false;
            state.authUser = null;
        }
    }
});

export const authActions = authSlice.actions;
export const userInfo = (state) => state.auth;

export default authSlice.reducer;